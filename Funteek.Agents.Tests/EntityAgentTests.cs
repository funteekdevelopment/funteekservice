﻿using Funteek.Agents;
using Funteek.BusinessLogic.Interfaces;
using Funteek.ServiceContract;
using Funteek.ServiceContract.ViewModels.Entity;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funteek.Agents.Tests
{
    [TestFixture]
    public class EntityAgentTests
    {
        private Mock<IEntityRepository> entityRepositoryMock;

        private IEntityAgent entityAgent;

        [SetUp]
        public void Setup()
        {
            var moc = new Mock<IEntityRepository>();

            var entityId = 234;

            moc.Setup(a => a.Entity(entityId)).Returns(new EntityModelView
                {
                    About = "About",
                    CreateUserID = 1,
                    Email = "email",
                    Fb = "fb",
                    Image = "image",
                    IsApproved = false,
                    IsBlocked = false,
                    IsDeleted = false,
                    IsOfficial = false,
                    IsWarn = false,
                    Phone = "phone",
                    Title = "title",
                    TypeEntity = ServiceContract.Enums.Type.bar,
                    WebSite = "webSite"
                });

            this.entityRepositoryMock = moc;

            this.entityAgent = new EntityAgent(this.entityRepositoryMock.Object);
        }
        
        [Test]
        public void EntityWarnThatExist()
        {
            //Arrange
            var entityId = 234;
            
            //Act
            this.entityAgent.EntityWarn(entityId);

            //Assert
            this.entityRepositoryMock.Verify(x => x.EntityWarn(It.IsAny<long>()), Times.Once);
        }

        [Test]
        public void EntityWarnThatNotExist()
        {
            //Arrange
            var entityId = 000;

            //Act
            this.entityAgent.EntityWarn(entityId);

            //Assert
            this.entityRepositoryMock.Verify(x => x.EntityWarn(It.IsAny<long>()), Times.Never);
        }

        [Test]
        public void EntityOfficialThatExist()
        {
            //Arrange
            var entityId = 234;

            //Act
            this.entityAgent.EntityOfficial(entityId);

            //Assert
            this.entityRepositoryMock.Verify(x => x.EntityOfficial(It.IsAny<long>()), Times.Once);
        }

        [Test]
        public void EntityOfficialThatNotExist()
        {
            //Arrange
            var entityId = 000;

            //Act
            this.entityAgent.EntityOfficial(entityId);

            //Assert
            this.entityRepositoryMock.Verify(x => x.EntityOfficial(It.IsAny<long>()), Times.Never);
        }

        [Test]
        public void EntityBlockedThatExist()
        {
            //Arrange
            var entityId = 234;
            var isBlock = true;

            //Act
            this.entityAgent.EntityBlocked(entityId, isBlock);

            //Assert
            this.entityRepositoryMock.Verify(x => x.EntityBlocked(It.IsAny<long>(), It.IsAny<bool>()), Times.Once);
        }

        [Test]
        public void EntityBlockedThatNotExist()
        {
            //Arrange
            var entityId = 000;
            var isBlock = true;

            //Act
            this.entityAgent.EntityBlocked(entityId, isBlock);

            //Assert
            this.entityRepositoryMock.Verify(x => x.EntityBlocked(It.IsAny<long>(), It.IsAny<bool>()), Times.Never);
        }
    }
}
