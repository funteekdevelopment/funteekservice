﻿using System;
using System.Collections.Generic;
using ODS.ServiceContract.DataContract;
using ODS.ServiceContract.ViewModels.Entity;
using Funteek.Common.Infrastructure;
using System.ServiceModel;
using Funteek.ServiceContract;
using Funteek.BusinessLogic.Interfaces;
using Funteek.BusinessLogic.Repositories;
using Funteek.Common.Logging;
using Funteek.BusinessLogic.Converters;

namespace Funteek.Agents
{
    /// <summary>
    /// The EntityAgent
    /// </summary>
    public class EntityAgent : IEntityAgent
    {
        #region Fields

        /// <summary>
        /// The _entityRepository
        /// </summary>
        private IEntityRepository _entityRepository;

        #endregion

        #region Property

        /// <summary>
        /// The entityRepository
        /// </summary>
        public IEntityRepository entityRepository
        {
            get
            {
                if (_entityRepository == null) this._entityRepository = new EntityRepository();

                return this._entityRepository;
            }
        }

        /// <summary>
        /// The logger
        /// </summary>
        private ILogger _logger;

        /// <summary>
        /// The Logger
        /// </summary>
        private ILogger logger
        {
            get
            {
                if (this._logger == null) this._logger = new Logger();
                
                return this._logger;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// The EntityAgent initializing without parameters
        /// </summary>
        public EntityAgent() { }

        /// <summary>
        /// The EntityAgent initializing
        /// </summary>
        /// <param name="entityRepository"></param>
        public EntityAgent(IEntityRepository entityRepository)
        {
            this._entityRepository = entityRepository;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Creaing new entity
        /// </summary>
        /// <param name="request"></param>
        public void Create(Funteek.ServiceContract.DataContract.EntityCreate request)
        {
            try
            {
                this.entityRepository.Create(EntityConverter.EntityCreateConvert(request));
            }
            catch (Exception ex)
            {
                //Logger.Error(Constants.InternalServerError, ex);

                var fault = new ErrorDetails(ex.Message, ex.StackTrace);

                throw new FaultException<ErrorDetails>(fault, new FaultReason(Constants.InternalServerError));
            }
        }

        /// <summary>
        /// List of thr all entities
        /// </summary>
        /// <returns></returns>
        public List<Funteek.ServiceContract.ViewModels.Entity.EntityAdminView> Entities()
        {
            try
            {
                var response = new List<Funteek.ServiceContract.ViewModels.Entity.EntityAdminView>();

                this.entityRepository.Entities();

                return response;
            }
            catch (Exception ex)
            {
                //Logger.Error(Constants.InternalServerError, ex);

                var fault = new ErrorDetails(ex.Message, ex.StackTrace);

                throw new FaultException<ErrorDetails>(fault, new FaultReason(Constants.InternalServerError));
            }
        }

        /// <summary>
        /// Get all non confirm entities
        /// </summary>
        /// <returns></returns>
        public List<Funteek.ServiceContract.ViewModels.Entity.EntityNonConfirmAdminView> EntitiesNonConfirm()
        {
            try
            {
                var response = new List<Funteek.ServiceContract.ViewModels.Entity.EntityNonConfirmAdminView>();

                this.entityRepository.EntitiesNonConfirm();

                return response;
            }
            catch (Exception ex)
            {
                //Logger.Error(Constants.InternalServerError, ex);

                var fault = new ErrorDetails(ex.Message, ex.StackTrace);

                throw new FaultException<ErrorDetails>(fault, new FaultReason(Constants.InternalServerError));
            }
        }

        /// <summary>
        /// Set for the entity value block/not block
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="isBlocked"></param>
        public void EntityBlocked(long entityId, bool isBlocked)
        {
            try
            {
                var entity = this.entityRepository.Entity(entityId);

                if (entity == null) { logger.Error($"entity with id  - [{entityId}] not found"); return; }

                this.entityRepository.EntityBlocked(entityId, isBlocked);
            }
            catch (Exception ex)
            {
                //Logger.Error(Constants.InternalServerError, ex);

                var fault = new ErrorDetails(ex.Message, ex.StackTrace);

                throw new FaultException<ErrorDetails>(fault, new FaultReason(Constants.InternalServerError));
            }
        }

        /// <summary>
        /// Making the company is official
        /// </summary>
        /// <param name="entityId"></param>
        public void EntityOfficial(long entityId)
        {
            try
            {
                var entity = this.entityRepository.Entity(entityId);

                if (entity == null) { logger.Error($"entity with id  - [{entityId}] not found"); return; }

                this.entityRepository.EntityOfficial(entityId);
            }
            catch (Exception ex)
            {
                //Logger.Error(Constants.InternalServerError, ex);

                var fault = new ErrorDetails(ex.Message, ex.StackTrace);

                throw new FaultException<ErrorDetails>(fault, new FaultReason(Constants.InternalServerError));
            }
        }

        /// <summary>
        /// Send warn for entity
        /// </summary>
        /// <param name="entityId"></param>
        public void EntityWarn(long entityId)
        {
            try
            {
                var entity = this.entityRepository.Entity(entityId);

                if (entity == null) { logger.Error($"entity with id  - [{entityId}] not found"); return; }

                this.entityRepository.EntityWarn(entityId);
            }
            catch (Exception ex)
            {
                logger.Error(Constants.InternalServerError, ex);

                var fault = new ErrorDetails(ex.Message, ex.StackTrace);

                throw new FaultException<ErrorDetails>(fault, new FaultReason(Constants.InternalServerError));
            }
        }

        #endregion
    }
}
