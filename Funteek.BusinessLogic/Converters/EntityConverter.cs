﻿using Funteek.ServiceContract.ViewModels;
using Funteek.ServiceContract.ViewModels.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funteek.BusinessLogic.Converters
{
    public class EntityConverter
    {
        public static List<EntityAdminView> EntityAdminViewConvert(List<ODS.ServiceContract.ViewModels.Entity.EntityAdminView> entities)
        {
            var result = new List<EntityAdminView>();

            result.AddRange(entities.Select(item => new EntityAdminView
            {
                id = item.id,
                count_complaint = item.count_complaint,
                image = item.image,
                is_official = item.is_official,
                is_warn = item.is_warn,
                name = item.name
            }));

            return result;
        }

        public static List<EntityNonConfirmAdminView> EntityNonConfirmAdminViewConvert(List<ODS.ServiceContract.ViewModels.Entity.EntityNonConfirmAdminView> entities)
        {
            var result = new List<EntityNonConfirmAdminView>();

            result.AddRange(entities.Select(item => new EntityNonConfirmAdminView
            {
                id = item.id,
                email = item.email,
                name_company = item.name_company,
                phone = item.phone,
                web_site = item.web_site
            }));

            return result;
        }

        public static ODS.ServiceContract.DataContract.EntityCreate EntityCreateConvert(ServiceContract.DataContract.EntityCreate entity)
        {
            var result = new ODS.ServiceContract.DataContract.EntityCreate
            {
                About = entity.About,
                CreateUserID = entity.CreateUserID,
                Email = entity.Email,
                Fb = entity.Fb,
                Image = entity.Image,
                Phone = entity.Phone,
                Title = entity.Title,
                Type = entity.Type,
                WebSite = entity.WebSite
            };

            return result;
        }

        public static EntityModelView EntityModelViewConvert(ODS.ServiceContract.ViewModels.Entity.EntityModelView entity)
        {
            var result = new ServiceContract.ViewModels.Entity.EntityModelView
            {
                About = entity.About,
                CreateUserID = entity.CreateUserID,
                Email = entity.Email,
                Fb = entity.Fb,
                Image = entity.Image,
                Phone = entity.Phone,
                Title = entity.Title,
                WebSite = entity.WebSite,
                IsApproved = entity.IsApproved,
                IsBlocked = entity.IsBlocked,
                IsDeleted = entity.IsDeleted,
                IsOfficial = entity.IsOfficial,
                IsWarn = entity.IsWarn,
                TypeEntity = (ServiceContract.Enums.Type)Enum.Parse(typeof(ODS.ServiceContract.Enums.Type), entity.TypeEntity.ToString())
            };

            return result;
        }
    }
}
