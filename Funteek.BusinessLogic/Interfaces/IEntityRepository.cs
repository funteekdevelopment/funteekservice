﻿using ODS.ServiceContract.DataContract;
using ODS.ServiceContract.ViewModels.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funteek.BusinessLogic.Interfaces
{
    /// <summary>
    /// The IEntityRepository
    /// </summary>
    public interface IEntityRepository
    {
        /// <summary>
        /// Creaing new entity
        /// </summary>
        /// <param name="request"></param>
        void Create(EntityCreate request);

        /// <summary>
        /// List of thr all entities
        /// </summary>
        /// <returns></returns>
        List<Funteek.ServiceContract.ViewModels.Entity.EntityAdminView> Entities();

        /// <summary>
        /// Get all non confirm entities
        /// </summary>
        /// <returns></returns>
        List<ServiceContract.ViewModels.Entity.EntityNonConfirmAdminView> EntitiesNonConfirm();

        /// <summary>
        /// Set for the entity value block/not block
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="isBlocked"></param>
        void EntityBlocked(long entityId, bool isBlocked);

        /// <summary>
        /// Making the company is official
        /// </summary>
        /// <param name="entityId"></param>
        void EntityOfficial(long entityId);

        /// <summary>
        /// Send warn for entity
        /// </summary>
        /// <param name="entityId"></param>
        void EntityWarn(long entityId);

        /// <summary>
        /// Get entity by id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        ServiceContract.ViewModels.Entity.EntityModelView Entity(long entityId);
    }
}
