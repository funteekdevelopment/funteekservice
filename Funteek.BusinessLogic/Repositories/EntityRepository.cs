﻿using Funteek.BusinessLogic.Interfaces;
using Funteek.Common.Clients;
using Funteek.Common.Infrastructure;
using ODS.ServiceContract;
using ODS.ServiceContract.DataContract;
using System.Collections.Generic;
using System;
using Funteek.ServiceContract.ViewModels.Entity;
using ODS.ServiceContract.ViewModels.Entity;
using Funteek.BusinessLogic.Converters;

namespace Funteek.BusinessLogic.Repositories
{
    /// <summary>
    /// The EntityRepository
    /// </summary>
    public class EntityRepository : IEntityRepository
    {
        #region Fields

        /// <summary>
        /// The client
        /// </summary>
        private readonly IClient<IEntityService> client;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializing EntityRepository
        /// </summary>
        /// <param name="client"></param>
        public EntityRepository(IClient<IEntityService> client)
        {
            this.client = client;
        }

        /// <summary>
        /// Initializing EntityRepository
        /// </summary>
        public EntityRepository() : this(new Client<IEntityService>(Constants.EntityServiceEndPointName))
        {
        }

        #endregion

        /// <summary>
        /// Creaing new entity
        /// </summary>
        /// <param name="request"></param>
        public void Create(EntityCreate request)
        {
            this.client.UseService(client => { client.Create(request); });
        }

        /// <summary>
        /// List of thr all entities
        /// </summary>
        /// <returns></returns>
        public List<ServiceContract.ViewModels.Entity.EntityAdminView> Entities()
        {
            List<ServiceContract.ViewModels.Entity.EntityAdminView> response = new List<ServiceContract.ViewModels.Entity.EntityAdminView>();

            this.client.UseService(client => { response = EntityConverter.EntityAdminViewConvert(client.Entities()); });

            return response;
        }

        /// <summary>
        /// Get all non confirm entities
        /// </summary>
        /// <returns></returns>
        public List<ServiceContract.ViewModels.Entity.EntityNonConfirmAdminView> EntitiesNonConfirm()
        {
            List<ServiceContract.ViewModels.Entity.EntityNonConfirmAdminView> response = new List<ServiceContract.ViewModels.Entity.EntityNonConfirmAdminView>();

            this.client.UseService(client => { response = EntityConverter.EntityNonConfirmAdminViewConvert(client.EntitiesNonConfirm()); });

            return response;
        }

        /// <summary>
        /// Set for the entity value block/not block
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="isBlocked"></param>
        public void EntityBlocked(long entityId, bool isBlocked)
        {
            this.client.UseService(client => { client.EntityBlocked(entityId, isBlocked); });
        }

        /// <summary>
        /// Making the company is official
        /// </summary>
        /// <param name="entityId"></param>
        public void EntityOfficial(long entityId)
        {
            this.client.UseService(client => { client.EntityOfficial(entityId); });
        }

        /// <summary>
        /// Send warn for entity
        /// </summary>
        /// <param name="entityId"></param>
        public void EntityWarn(long entityId)
        {
            this.client.UseService(client => { client.EntityWarn(entityId); });
        }

        /// <summary>
        /// Get entity by id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public ServiceContract.ViewModels.Entity.EntityModelView Entity(long entityId)
        {
            ServiceContract.ViewModels.Entity.EntityModelView entity = null;

            this.client.UseService(client => { entity = EntityConverter.EntityModelViewConvert(client.Entity(entityId)); });

            return entity; 
        }
    }
}
