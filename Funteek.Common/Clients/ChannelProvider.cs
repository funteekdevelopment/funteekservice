﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Funteek.Common.Clients
{
    /// <summary>
    /// Abstraction over <see cref="ChannelFactory{TChannel}"/>.
    /// </summary>
    /// <typeparam name="TService">Contract.</typeparam>
    public sealed class ChannelProvider<TService> : IChannelProvider<TService>
    {
        #region Fields

        /// <summary>
        /// Channel factory.
        /// </summary>
        private readonly ChannelFactory<TService> factory;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ChannelProvider{TService}" /> class.
        /// </summary>
        /// <param name="endpointConfigurationName">Name of the endpoint configuration.</param>
        public ChannelProvider(string endpointConfigurationName)
        {
            this.factory = new ChannelFactory<TService>(endpointConfigurationName);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Creatin chanel
        /// </summary>
        /// <returns></returns>
        public TService CreateChannel()
        {
            return this.factory.CreateChannel();
        }

        #endregion
    }
}
