﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Funteek.Common.Clients
{
    /// <summary>
    /// The Client
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    public class Client<TService> : IClient<TService>
    {
        #region Fields

        /// <summary>
        /// The channelProvider
        /// </summary>
        private readonly IChannelProvider<TService> channelProvider;

        /// <summary>
        /// The _lock
        /// </summary>
        private readonly object _lock = new object();

        /// <summary>
        /// The contractName
        /// </summary>
        private readonly string contractName;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializing Client
        /// </summary>
        /// <param name="channelProvider"></param>
        public Client(IChannelProvider<TService> channelProvider)
        {
            this.channelProvider = channelProvider;
            this.contractName = typeof(TService).Name;
        }

        /// <summary>
        /// Initializing Client
        /// </summary>
        /// <param name="endpointConfigurationName"></param>
        public Client(string endpointConfigurationName) : this(new ChannelProvider<TService>(endpointConfigurationName))
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Method for invoke metods from services
        /// </summary>
        /// <param name="procedure"></param>
        public void UseService(Action<TService> procedure)
        {
            this.InvokeService(s => { procedure(s); return 0; });
        }

        /// <summary>
        /// Method for invoke metods from services
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="function"></param>
        /// <returns></returns>
        public TResult UseService<TResult>(Func<TService, TResult> function)
        {
            return this.InvokeService(function);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Method for invoke metods from services
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="action"></param>
        /// <returns></returns>
        private TResult InvokeService<TResult>(Func<TService, TResult> action)
        {
            lock (this._lock)
            {
                var proxy = this.channelProvider.CreateChannel();
                TResult result = default(TResult);
                try
                {
                    result = action(proxy);
                }
                finally
                {
                    this.CloseOrAbortServiceChannel((ICommunicationObject)proxy);
                
                    proxy = default(TService);
                }
                return result;
            }
        }

        /// <summary>
        /// Method for close or abort service channel
        /// </summary>
        /// <param name="communicationObject"></param>
        private void CloseOrAbortServiceChannel(ICommunicationObject communicationObject)
        {
            bool isClosed = false;

            if (communicationObject == null || communicationObject.State == CommunicationState.Closed)
            {
                return;
            }

            try
            {
                if (communicationObject.State != CommunicationState.Faulted)
                {
                    communicationObject.Close();
                    isClosed = true;
                }
            }
            catch (Exception ex)
            {
                //this.logger.Error($"Exception while closing communication object for {this.contractName}", ex);
            }
            finally
            {
                // If State was Faulted or any exception occurred while doing the Close(), then do an Abort()
                if (!isClosed)
                {
                    this.AbortServiceChannel(communicationObject);
                }
            }
        }

        /// <summary>
        /// Method for abort services
        /// </summary>
        /// <param name="communicationObject"></param>
        private void AbortServiceChannel(ICommunicationObject communicationObject)
        {
            try
            {
                communicationObject.Abort();
            }
            catch (Exception ex)
            {
                //this.logger.Error($"Exception while aborting communication object for {this.contractName}", ex);
            }
        }

        #endregion
    }
}
