﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funteek.Common.Clients
{
    /// <summary>
    /// Abstraction over <see cref="ChannelFactory{TChannel}"/>.
    /// </summary>
    /// <typeparam name="TService">Service contract.</typeparam>
    public interface IChannelProvider<out TService>
    {
        /// <summary>
        /// Creates <see cref="TService"/> object for remote service call.
        /// </summary>
        /// <returns>
        ///    <see cref="IClientChannel"/> of <see cref="TService"/>.
        /// </returns>
        TService CreateChannel();
    }
}
