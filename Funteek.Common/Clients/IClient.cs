﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funteek.Common.Clients
{
    /// <summary>
    /// Unified remote service invoker.
    /// </summary>
    /// <typeparam name="TService">The type of the service.</typeparam>
    public interface IClient<out TService>
    {
        /// <summary>
        /// Safe invoke of service method with non-void return type.
        /// </summary>
        /// <param name="function">Service call.</param>
        /// <typeparam name="TResult">Server response object type.</typeparam>
        /// <returns>Service response.</returns>
        TResult UseService<TResult>(Func<TService, TResult> function);

        /// <summary>
        /// Safe invoke of service method with void return type.
        /// </summary>
        /// <param name="procedure">Service call.</param>
        void UseService(Action<TService> procedure);
    }
}
