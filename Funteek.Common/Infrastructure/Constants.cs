﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funteek.Common.Infrastructure
{
    /// <summary>
    /// Constants
    /// </summary>
    public class Constants
    {
        #region Constants

        /// <summary>
        /// The EntityServiceEndPointName
        /// </summary>
        public const string EntityServiceEndPointName = "EntityServiceClient";

        /// <summary>
        /// The InternalServerError
        /// </summary>
        public const string InternalServerError = "InternalServerError";

        /// <summary>
        /// The ClientArgumentError
        /// </summary>
        public const string ClientArgumentError = "ClientArgumentError";

        #endregion
    }
}
