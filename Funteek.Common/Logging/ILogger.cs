﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funteek.Common.Logging
{
    /// <summary>
    /// The ILogger
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// The Error
        /// </summary>
        /// <param name="msg"></param>
        void Error(string msg);

        /// <summary>
        /// The Error
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="ex"></param>
        void Error(string msg, Exception ex);

        /// <summary>
        /// The ErrorFormat
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="args"></param>
        void ErrorFormat(string msg, params object[] args);

        /// <summary>
        /// The Info
        /// </summary>
        /// <param name="msg"></param>
        void Info(string msg);

        /// <summary>
        /// Debug method
        /// </summary>
        /// <param name="format"> format string</param>
        /// <param name="parameters">parameters array</param>
        void Debug(string format);
    }
}
