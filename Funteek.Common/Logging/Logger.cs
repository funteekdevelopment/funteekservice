﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funteek.Common.Logging
{
    /// <summary>
    /// The Logger
    /// </summary>
    public class Logger : ILogger
    {
        #region Private Fields

        /// <summary>
        /// The logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(Logger));

        #endregion

        #region Public Methods

        /// <summary>
        /// The Error
        /// </summary>
        /// <param name="msg"></param>
        public void Error(string msg)
        {
            logger.Error(msg);
        }

        /// <summary>
        /// The Error
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="ex"></param>
        public void Error(string msg, Exception ex)
        {
            logger.Error(msg, ex);
        }

        /// <summary>
        /// The ErrorFormat
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="args"></param>
        public void ErrorFormat(string msg, params object[] args)
        {
            logger.ErrorFormat(msg, args);
        }

        /// <summary>
        /// The Info
        /// </summary>
        /// <param name="msg"></param>
        public void Info(string msg)
        {
            logger.Info(msg);
        }

        /// <summary>
        /// Debug method : forwards the call to underlying Log object
        /// </summary>
        /// <param name="format"> format string</param>
        /// <param name="parameters">parameters array</param>
        public void Debug(string format)
        {
            logger.Debug(format);
        }

        #endregion
    }
}
