﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Funteek.ServiceContract.DataContract
{
    [DataContract]
    public class EntityCreate
    {
        [DataMember(IsRequired = true)]
        public long CreateUserID { get; set; }

        [DataMember(IsRequired = true)]
        public string Image { get; set; }

        [DataMember(IsRequired = true)]
        public string Title { get; set; }

        [DataMember(IsRequired = true)]
        public string WebSite { get; set; }

        [DataMember(IsRequired = true)]
        public string Phone { get; set; }

        [DataMember(IsRequired = true)]
        public string About { get; set; }

        [DataMember(IsRequired = true)]
        public string Type { get; set; }

        [DataMember(IsRequired = true)]
        public string Email { get; set; }

        [DataMember(IsRequired = true)]
        public string Fb { get; set; }
    }
}
