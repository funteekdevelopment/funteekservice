﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Funteek.ServiceContract.DataContract
{
    /// <summary>
    /// This class is to be used for communicating the error details to the clients.
    /// </summary>
    [DataContract]
    public class ErrorDetails
    {
        /// <summary>
        /// Gets or sets the human understandable high-level description of the error.
        /// </summary>
        [DataMember]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the description field.This could contain more details like the exact nature of the error 
        /// or stack trace etc.
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        public ErrorDetails(string message, string description)
        {
            this.Message = message;
            this.Description = description;
        }
    }
}
