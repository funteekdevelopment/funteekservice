﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funteek.ServiceContract.Enums
{
    public enum Type
    {
        restaurant = 1,
        coffee = 2,
        bar = 3,
        brand = 4,
        community = 5,
        sports = 6,
        auto = 7,
        tools = 8,
        fun = 9,
        purchases = 10
    }
}
