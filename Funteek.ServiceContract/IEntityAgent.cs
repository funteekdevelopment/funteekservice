﻿using Funteek.ServiceContract.DataContract;
using Funteek.ServiceContract.ViewModels.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Funteek.ServiceContract
{
    /// <summary>
    /// The IEntityAgent
    /// </summary>
    [ServiceContract]
    public interface IEntityAgent
    {
        #region Public Methods SuperAdmin Part

        /// <summary>
        /// set for the entity value block/not block
        /// </summary>
        /// <param name="entityId">The entityId</param>
        /// <param name="isBlocked">The isBlocked</param>
        [OperationContract]
        [FaultContract(typeof(ErrorDetails))]
        void EntityBlocked(long entityId, bool isBlocked);

        /// <summary>
        /// making the company is official
        /// </summary>
        /// <param name="entityId">The entityId</param>
        [OperationContract]
        [FaultContract(typeof(ErrorDetails))]
        void EntityOfficial(long entityId);

        /// <summary>
        /// Send warn for entity
        /// </summary>
        /// <param name="entityId"></param>
        [OperationContract]
        [FaultContract(typeof(ErrorDetails))]
        void EntityWarn(long entityId);

        /// <summary>
        /// List of thr all entities
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(ErrorDetails))]
        List<EntityAdminView> Entities();

        /// <summary>
        /// Get non confirm entities
        /// </summary>
        /// <param name="pagination">The pagination</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(ErrorDetails))]
        List<EntityNonConfirmAdminView> EntitiesNonConfirm();

        /// <summary>
        /// Approve entity
        /// </summary>
        /// <param name="entityNonAprovedId"></param>
        /// <returns></returns>
        //EntityAprovedView EntityAproved(long entityNonAprovedId);

        #endregion

        #region Public Methods Web Part

        /// <summary>
        /// Create new entity
        /// </summary>
        /// <param name="request"></param>
        [OperationContract]
        [FaultContract(typeof(ErrorDetails))]
        void Create(EntityCreate request);

        #endregion
    }
}
