﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funteek.ServiceContract.ViewModels.Entity
{
    public class EntityAdminView
    {
        public long id { get; set; }

        public string image { get; set; }

        public string name { get; set; }

        public bool is_official { get; set; }

        public int count_complaint { get; set; }

        public bool is_warn { get; set; }
    }
}
