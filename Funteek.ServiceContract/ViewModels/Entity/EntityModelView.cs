﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funteek.ServiceContract.ViewModels.Entity
{
    public class EntityModelView
    {
        public string About { get; set; }
        public long CreateUserID { get; set; }
        public string Email { get; set; }
        public string Fb { get; set; }
        public string Image { get; set; }
        public bool IsApproved { get; set; }
        public bool IsBlocked { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsOfficial { get; set; }
        public bool IsWarn { get; set; }
        public string Phone { get; set; }
        public string Title { get; set; }
        public Enums.Type TypeEntity { get; set; }
        public string WebSite { get; set; }
    }
}
