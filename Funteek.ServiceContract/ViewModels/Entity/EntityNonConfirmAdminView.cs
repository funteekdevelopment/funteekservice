﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funteek.ServiceContract.ViewModels.Entity
{
    public class EntityNonConfirmAdminView
    {
        public long id { get; set; }

        public string name_company { get; set; }

        public string web_site { get; set; }

        public string email { get; set; }

        public string phone { get; set; }
    }
}
