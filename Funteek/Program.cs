﻿using Autofac;
using Autofac.Configuration;
using Funteek.BusinessLogic.Interfaces;
using Funteek.ServiceContract;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Funteek
{
    /// <summary>
    /// The Program
    /// </summary>
    class Program
    {
        /// <summary>
        /// The Main
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            // configuring logger
            // test
            log4net.Config.XmlConfigurator.Configure();

            using (var host = new ServiceHost(typeof(Agents.EntityAgent)))
            {
                host.Open();

                Console.WriteLine("Host started...");
                Console.ReadLine();
            }
        }
    }
}
